from sklearn import svm

class SupportVectorMachine(object):
  def __init__(self, test_lines_covered, test_results,
               source_file_total_lines, MAX_EXECUTED_TESTS):
    self.test_lines_covered = test_lines_covered
    self.test_results = test_results
    self.source_file_total_lines = source_file_total_lines
    self.max_executed_tests = MAX_EXECUTED_TESTS
    self.pre_processed_test_results = []
    self.clf = None

    self.pre_process_data()

  def pre_process_data(self):
    new_data = []
    for i in range(self.max_executed_tests):
      new_data.append([])
      for j in range(self.source_file_total_lines):
        new_data[i].append(0)

    for a, b in zip(new_data, self.test_lines_covered):
      for l in b:
        a[l-1] = l

    self.pre_processed_test_results = new_data

  def fit_SVM_model(self):
    self.clf = svm.SVC(C=len(self.pre_processed_test_results), kernel='linear', probability=True)

    if len(set(self.test_results)) > 1: # Must have different test result values (both 0 and 1)
      self.clf.fit(self.pre_processed_test_results, self.test_results)
      return True
    else:
      return False

  def make_prediction(self, unknown_test_case_lineno_results):
    print(self.clf.predict(unknown_test_case_lineno_results))

  def get_prediction_accuracy(self):
    return self.clf.score(self.pre_processed_test_results, self.test_results)

  def print_pre_processed_test_results(self):
    for pre_processed_test_result in self.pre_processed_test_results:
      print(pre_processed_test_result)

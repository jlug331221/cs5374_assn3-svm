import os, sys
from profiler import Profiler
import test_suite as ts
from random import randrange
import svm_wrapper as svm_w

source_file = os.path.join(os.getcwd(), 'source.py')

'''
# Generate test lines covered for a new test number; omit the test results for prediction.
'''
def generate_new_test_case():
  stc = ts.SimpleTestClass()
  test_lines_covered = []

  profiler = Profiler(source_file)
  source_file_total_lines = len(profiler.source_code)

  prime_to_test = randrange(2, 101)  # random prime number from 2 to 100

  sys.settrace(profiler.trace)

  stc.test_is_prime(prime_to_test)

  sys.settrace(None)

  test_lines_covered.append(list(profiler.test_coverage_info.values())[0])

  pp_test_lines_covered = []
  pp_test_lines_covered.append([])
  for i in range(source_file_total_lines):
    pp_test_lines_covered[0].append(0)

  for a, b in zip(pp_test_lines_covered, test_lines_covered):
    for l in b:
      a[l-1] = l

  # print(test_lines_covered)
  print('New test case lines covered for prime number ' + str(prime_to_test) + ': ' +
        str(pp_test_lines_covered))
  return pp_test_lines_covered

'''
  1. Run MAX_EXCUTED_TESTS to generate line coverage data about test execution
  2. Instantiate SVM wrapper object
  3. Fit the model
  4. Make prediction on new test case
'''
def main():
  MAX_EXECUTED_TESTS = 10

  stc = ts.SimpleTestClass()
  test_lines_covered = []
  test_results = []

  profiler = Profiler(source_file)
  source_file_total_lines = len(profiler.source_code)

  i = 0
  while i < MAX_EXECUTED_TESTS:
    prime_to_test = randrange(2, 101) # random prime number from 2 to 100

    sys.settrace(profiler.trace)

    if(stc.test_is_prime(prime_to_test)):
      test_results.append(1)
    else:
      test_results.append(0)

    sys.settrace(None)

    test_lines_covered.append(list(profiler.test_coverage_info.values())[0])

    if i == 9:
      print('T' + str(i+1) + ':  number to test = ' + str(prime_to_test) + '\t->\t' +
            str(profiler.test_coverage_info))
    else:
      print('T' + str(i + 1) + ':   number to test = ' + str(prime_to_test) + '\t->\t' +
            str(profiler.test_coverage_info))

    profiler = Profiler(source_file)

    i += 1

  print('\nInitial test results:')
  print(test_results)

  print('\nForming SVM model...\n')

  classifier = svm_w.SupportVectorMachine(test_lines_covered, test_results,
                                          source_file_total_lines, MAX_EXECUTED_TESTS)

  # classifier.print_pre_processed_test_results()

  if classifier.fit_SVM_model():
    unknown_test_case_lineno_results = generate_new_test_case()

    print('Test Case Prediction: ', end='')
    classifier.make_prediction(unknown_test_case_lineno_results)

    prediction_accuracy = classifier.get_prediction_accuracy()

    print('\nPrediction accuracy: ' + str(prediction_accuracy))
  else:
    print('** Unable to fit model because test results were all the same (all 0s or all 1s). **')

if __name__ == '__main__':
  main()
import inspect

class Profiler(object):
  def __init__(self, source_file_to_watch):
    self.source_file = source_file_to_watch
    self.source_code = open(self.source_file).readlines()
    self.test_coverage_info = dict()
    self.total_lines_covered = []
    self.tested_function_names = []

  '''
  #
  # Trace the frame and event to extract the executed function name and obtain the line number.
  #
  '''
  def trace(self, frame, event, arg):
    current_file = inspect.getframeinfo(frame).filename
    function_name = inspect.getframeinfo(frame).function

    if self.source_file in current_file and (event == 'line' or event == 'call'):
      self.total_lines_covered.append(frame.f_lineno)

      key = 'test_' + function_name

      if key not in self.test_coverage_info:
        self.test_coverage_info[key] = []
        if frame.f_lineno not in self.test_coverage_info[key]:
          self.test_coverage_info[key].append(frame.f_lineno)
      else:
        if frame.f_lineno not in self.test_coverage_info[key]:
          self.test_coverage_info[key].append(frame.f_lineno)

      if event == 'call':
        if function_name not in self.tested_function_names:
          self.tested_function_names.append(function_name)

    return self.trace

  '''
  #
  # Prints the total lines covered by the executed test cases, names of the functions executed
  # and the lines executed for each test case.
  #
  '''
  def report_results(self):
    print('Test cases for \'' + self.source_file + '\', covers lines:')
    for line_covered in self.total_lines_covered:
      print(str(line_covered) + ' ', end='')

    print('\n\nTest suite covered function names:')
    for function_name in self.tested_function_names:
      print(function_name)

    print('\nLines covered per test:')
    for key in self.test_coverage_info.keys():
      print(key + ': ', end='')
      for line_no in self.test_coverage_info[key]:
        print(str(line_no) + ' ', end='')
      print()

  '''
  #
  # Add line numbers to the source file under test and store the output in
  # './source_file_with_line_numbers.txt'
  #
  '''
  def add_line_numbers_to_source_file(self):
    with open('source_file_with_line_numbers.txt', 'w') as file_with_line_numbers:
      for i, line in enumerate(self.source_code):
        file_with_line_numbers.write(str(i+1) + '\t ' + line)